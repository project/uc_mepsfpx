#!/bin/bash
#
# chkconfig: 345 99 05 
# description: MEPS FPX Init
#
# A Red Hat Enterprise Linux start/stop script for MEPS FPX Java daemons.
# Copyright 2012-2013 Mohd Khalemi Ab Khalid, Absec Malaysia Sdn Bhd, khalemi@absecmy.com, mohd@khalemi.net
# This script execute FPX plgin as a user with less privilege.
# To do this you must create a local account (fpxd) with for use with the plugins.

# CONTOH
# JAVA_HOME="/usr/lib/jvm/java-1.6.0-openjdk"
# serviceUserHome = "/home/fpxd"
# applDir = "$serviceUserHome/EX00000000/fpx"

# CONFIG

JAVA_HOME="{PATH KAT MANA HG INSTALL JAVA}"                    
serviceUserHome="{HOME DIRECTORY UNTUK USER FPX}"
applDir="$serviceUserHome/{PATH KAT MANA HG INSTALL FPX}"

serviceNameLo="fpxd"					
serviceName="MEPS FPX Plugin Daemon"					
serviceUser="fpxd"						
serviceGroup="fpxd"						

# NO CONFIG BELOW THIS LINE


maxShutdownTime=15                                         
serviceLogFile="$applDir/fpx.service.log"             
pidFile="$applDir/$serviceNameLo.pid"                     
javaCommand="java"                                        
javaExe="$JAVA_HOME/bin/$javaCommand"				
javaArgs="com.fpx.seller.daemon.PluginDaemon"                 
javaCommandLine="$javaExe $javaArgs"				
javaCommandLineKeyword="com.fpx.seller.daemon.PluginDaemon"            

# Makes the file $1 writable by the group $serviceGroup.
function makeFileWritable {
   local filename="$1"
   touch $filename || return 1
   chgrp $serviceGroup $filename || return 1
   chmod g+w $filename || return 1
   return 0; }

# Returns 0 if the process with PID $1 is running.
function checkProcessIsRunning {
   local pid="$1"
   if [ -z "$pid" -o "$pid" == " " ]; then return 1; fi
   if [ ! -e /proc/$pid ]; then return 1; fi
   return 0; }

# Returns 0 if the process with PID $1 is our Java service process.
function checkProcessIsOurService {
   local pid="$1"
   if [ "$(ps -p $pid --no-headers -o comm)" != "$javaCommand" ]; then return 1; fi
   grep -q --binary -F "$javaCommandLineKeyword" /proc/$pid/cmdline
   if [ $? -ne 0 ]; then return 1; fi
   return 0; }

# Returns 0 when the service is running and sets the variable $pid to the PID.
function getServicePID {
   if [ ! -f $pidFile ]; then return 1; fi
   pid="$(<$pidFile)"
   checkProcessIsRunning $pid || return 1
   checkProcessIsOurService $pid || return 1
   return 0; }

function startServiceProcess {
   cd $applDir || return 1
   rm -f $pidFile
   makeFileWritable $pidFile || return 1
   makeFileWritable $serviceLogFile || return 1

#we add the fpx class here
export PLUGIN_LIB=$applDir/lib
export CLASSPATH=$PLUGIN_LIB/activation.jar
export CLASSPATH=$CLASSPATH:$PLUGIN_LIB/clone.jar
export CLASSPATH=$CLASSPATH:$PLUGIN_LIB/extra.jar
export CLASSPATH=$CLASSPATH:$PLUGIN_LIB/jca.jar
export CLASSPATH=$CLASSPATH:$PLUGIN_LIB/jce.jar
export CLASSPATH=$CLASSPATH:$PLUGIN_LIB/jcert.jar
export CLASSPATH=$CLASSPATH:$PLUGIN_LIB/jcrypto.jar
export CLASSPATH=$CLASSPATH:$PLUGIN_LIB/jssl.jar
export CLASSPATH=$CLASSPATH:$PLUGIN_LIB/log4j-1.2.8.jar
export CLASSPATH=$CLASSPATH:$PLUGIN_LIB/looks-1.2.1.jar
export CLASSPATH=$CLASSPATH:$PLUGIN_LIB/images.jar
export CLASSPATH=$CLASSPATH:$PLUGIN_LIB/looks-1.1.3.jar
export CLASSPATH=$CLASSPATH:$PLUGIN_LIB/mail.jar
export CLASSPATH=$CLASSPATH:$PLUGIN_LIB/sell_plugin.jar
export CLASSPATH=$CLASSPATH:$PLUGIN_LIB/images.jar
export CLASSPATH=$CLASSPATH:$PLUGIN_LIB/bcprov-jdk14-131.jar

   cmd="nohup $javaCommandLine >>$serviceLogFile 2>&1 & echo \$! >$pidFile"
   su -m $serviceUser -s $SHELL -c "$cmd" || return 1
   sleep 0.1
   pid="$(<$pidFile)"
   if checkProcessIsRunning $pid; then :; else
      echo -ne "\n$serviceName start failed, see logfile."
      return 1
   fi
   return 0; }

function stopServiceProcess {
   kill $pid || return 1
   for ((i=0; i<maxShutdownTime*10; i++)); do
      checkProcessIsRunning $pid
      if [ $? -ne 0 ]; then
         rm -f $pidFile
         return 0
         fi
      sleep 0.1
      done
   echo -e "\n$serviceName did not terminate within $maxShutdownTime seconds, sending SIGKILL..."
   kill -s KILL $pid || return 1
   local killWaitTime=15
   for ((i=0; i<killWaitTime*10; i++)); do
      checkProcessIsRunning $pid
      if [ $? -ne 0 ]; then
         rm -f $pidFile
         return 0
         fi
      sleep 0.1
      done
   echo "Error: $serviceName could not be stopped within $maxShutdownTime+$killWaitTime seconds!"
   return 1; }

function startService {
   getServicePID
   if [ $? -eq 0 ]; then echo -n "$serviceName is already running"; RETVAL=0; return 0; fi
   echo -n "Starting $serviceName   "
   startServiceProcess
   if [ $? -ne 0 ]; then RETVAL=1; echo "failed"; return 1; fi
   echo "started PID=$pid"
   RETVAL=0
   return 0; }

function stopService {
   getServicePID
   if [ $? -ne 0 ]; then echo -n "$serviceName is not running"; RETVAL=0; echo ""; return 0; fi
   echo -n "Stopping $serviceName   "
   stopServiceProcess
   if [ $? -ne 0 ]; then RETVAL=1; echo "failed"; return 1; fi
   echo "stopped PID=$pid"
   RETVAL=0
   return 0; }

function checkServiceStatus {
   echo -n "Checking for $serviceName:   "
   if getServicePID; then
	echo "running PID=$pid"
	RETVAL=0
   else
	echo "stopped"
	RETVAL=3
   fi
   return 0; }

function main {
   RETVAL=0
   case "$1" in
      start)                                              
         startService
         ;;
      stop)                                             
         stopService
         ;;
      restart)                                         
         stopService && startService
         ;;
      status)                                             
         checkServiceStatus
         ;;
      *)
         echo "Usage: $0 {start|stop|restart|status}"
         exit 1
         ;;
      esac
   exit $RETVAL
}

main $1
