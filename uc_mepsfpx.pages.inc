<?php
/**
 * @file
 * RETURN FROM FPX
 */


function uc_mepsfpx_complete($order_id = 0) {
  $_debug = variable_get('uc_mepsfpx_debug', FALSE);

  $errmesg = t('We are sorry. An error occurred while processing your order that prevents us from completing it at this time. Please contact us and we will resolve the issue as soon as possible.');

  watchdog('mepsfpx', 'Receiving MEPS FPX notification for order @order_id. <pre>@debug</pre>',
            array('@order_id' => $order_id, '@debug' => print_r($_POST, TRUE)));


  //+++++++++++++ start capture from FPX +++++++++++++++ //

$mepsFpx = new MepsFpx(variable_get('uc_mepsfpx_sid',''), 
			variable_get('uc_mepsfpx_hostname',''), 
			variable_get('uc_mepsfpx_port',''),
			variable_get('uc_mepsfpx_payment_mode', ''));

	$response = $mepsFpx->getResponse();

	// map response to local variable

	$sid		= check_plain($mepsFpx->getSellerId());
	$rorderid	= check_plain($response['message.orderno']);
	$dauth		= check_plain($response['debit.authcode']);
	$cauth		= check_plain($response['credit.authcode']);
	$curr		= check_plain($response['message.currency']);
	$txt_time	= check_plain($response['message.txnTime']);
	$fpx_tid	= check_plain($response['message.fpxTransactionId']);
	$amount		= check_plain($response['message.amount']);

	//extra
	
	$cauthno		= check_plain($response['credit.authno']);
	$dauthno		= check_plain($response['debit.authno']);
	$buyerid		= check_plain($response['buyer.id']);
	$buyerbank		= check_plain($response['buyer.bank']);
	$buyerbranch	= check_plain($response['buyer.bankbranch']);
	$buyeriban		= check_plain($response['buyer.iban']);
	$makername		= check_plain($response['maker.name']);
	$buyername		= check_plain($response['buyer.name']);
	

   // optional receipt number 8 digits
   $response['receipt.no'] = '600101'.str_pad($response['message.orderno'], 8, '0', STR_PAD_LEFT);

  /*
  if (!isset($_SERVER['HTTP_REFERER']) || !$_SERVER['HTTP_REFERER']) {
    
	// If no referer, we assume it is direct access. Block it!
    drupal_set_message('error referer', 'error');
    drupal_goto('cart');
  }
  */

  $payment_url = $mepsFpx->getPaymentUrl();
  $parsed_payment_url = parse_url($payment_url);
  $referer = parse_url($_SERVER['HTTP_REFERER']);
  if ($referer['host'] != $parsed_payment_url['host']) {
    
	// If referer is not coming from MEPS FPX server (dev or prod depending on settings) then block it, too!
    drupal_set_message('error referer 2', 'error');
    drupal_goto('cart');
  }

  // Now check if the required arguments exists.
  $required_args = array('mesgFromFpx');
  foreach ($required_args as $val) {
    if (!in_array($val, array_keys($_POST))) {
	drupal_set_message('error tak jumpa mesgFromFpx', 'error');
    drupal_goto('cart');
    }
  }

  // Order ID must exists in response!
  if (!$response['message.orderno']) {
    watchdog('uc_mepsfpx', 'order_id is empty or invalid. Debug info : ' . json_encode($response), array(), WATCHDOG_ERROR);
	drupal_set_message('error order is invalid', 'error');
    drupal_goto('cart');
  }

	if ($_debug) watchdog('mepsfpx', '<pre>@details</pre>', array('@details' => print_r($response, TRUE)), WATCHDOG_DEBUG);


 // Successful transaction
 if (($response['debit.authcode']=='00') || ($response['credit.authcode']=='00') ) {
	$order = uc_order_load($response['message.orderno']);

 //+++++++++++++ end capture from FPX +++++++++++++++ //

      watchdog('mepsfpx', 'mepsfpx transaction verified for order !order_id.', array('!order_id' => $orderid));

      $comment = t('Transaction ID #!tran_id, Bank Approval #!appcode).',
                    array('!date' => $txt_time, '!tran_id' => $fpx_tid, '!appcode' => $dauth.'&'.$cauth));

      uc_payment_enter($order->order_id, 'FPX', $amount, $order->uid, $response, $comment);


      $context = array(
        'revision' => 'original',
        'location' => 'mepsfpx-form',
      );

      $options = array(
        'sign' => variable_get('uc_currency_sign', 'RM'),
        'dec' => '.',
        'thou' => variable_get('uc_currency_thou', ','),
      );

      $output = uc_cart_complete_sale($order);

		
		//save comment for user
		
		uc_order_comment_save($order->order_id, 0, t('MyClear Reported Payment of RM !amount received via MEPS FPX.', array('!amount' => uc_price($amount, $context, $options))),'order','payment_received');

		uc_order_comment_save($order->order_id, 0, t('FPX Transaction ID :!detail2. Bank Approval Code : !bapproval .', array('!detail2' => $fpx_tid, '!bapproval' => $dauthno)),'order','completed');

		//admin comment
		//uc_order_comment_save($order->order_id, 0, t('FPX Raw Response !debug1.', array('!debug1' => '<pre>'.print_r($response, TRUE).'</pre>')));

      $page = variable_get('uc_cart_checkout_complete_page', '');
      if (!empty($page)) {
        drupal_goto(variable_get('uc_cart_checkout_complete_page', ''));
      }

      return $output;

  }
  else {
    watchdog('mepsfpx', 'Unsuccessful mepsfpx transaction for order !order_id.', array('!order_id' => $orderid), WATCHDOG_ERROR);
    uc_order_comment_save($orderid, 0, t('A mepsfpx transaction was not successful for this order.'), 'admin');
    drupal_set_message($errmsg, 'error');
    drupal_goto('cart');
  }
}

// todo
function uc_mepsfpx_dcomplete($order_id = 0) {
 
//set POST variables
$url = '';

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);

curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);

$result = curl_exec($ch);

curl_close($ch);



}